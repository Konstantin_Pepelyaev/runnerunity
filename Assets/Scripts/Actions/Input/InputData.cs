﻿using UnityEngine;

namespace Assets.Scripts.Actions.AI
{
    public class InputData : MonoBehaviour
    {
        [HideInInspector]
        public bool Jump = false;

        [HideInInspector]
        public float DodgeAmount = 0f;
    }
}
