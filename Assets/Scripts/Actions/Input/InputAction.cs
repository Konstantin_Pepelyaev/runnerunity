﻿using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.AI
{
    [CreateAssetMenu(menuName = "Runner/Actions/InputAction")]
    public class InputAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var data = GetComponent<InputData>(state);

            data.DodgeAmount = Input.GetAxisRaw("Horizontal");
            data.Jump = Input.GetAxisRaw("Vertical") > 0.1f;

            return true;
        }
    }
}
