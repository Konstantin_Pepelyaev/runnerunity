﻿using UnityEngine;

namespace Assets.Scripts.Actions.Jump
{
    public class JumpData : MonoBehaviour
    {
        public float Height = 14.0f;
        public float Speed = 5.0f;

        public Vector3 JumpAmount = Vector3.zero;

        [HideInInspector]
        public bool Jumping = false;
    }
}
