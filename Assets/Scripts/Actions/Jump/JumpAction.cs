﻿using Assets.Scripts.Actions.AI;
using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.Jump
{
    [CreateAssetMenu(menuName = "Runner/Actions/Jump")]
    public class JumpAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var data = GetComponent<JumpData>(state);

            var c = GetComponent<CharacterController>(state);
            if(c.isGrounded)
            {  
                data.JumpAmount.y = data.Height;
                data.JumpAmount.z = data.Speed;
                data.Jumping = true;
            }
            
            data.JumpAmount.y -= data.Height * blackboard.DeltaTime;
            c.Move(data.JumpAmount * blackboard.DeltaTime);
               
            return true;
        }
    }
}
