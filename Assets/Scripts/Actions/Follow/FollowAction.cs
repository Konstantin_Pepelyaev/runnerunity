﻿using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.Follow
{    
    [CreateAssetMenu(menuName = "Runner/Actions/Follow")]
    public class FollowAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var data = GetComponent<FollowData>(state);

            if(data.LookAt == null)
            {
                var target = GameObject.FindGameObjectWithTag(data.Target);
                data.LookAt = target.transform; 
            }

            if(data.StartOffset == null)
            {
                data.StartOffset = state.Transform.position - data.LookAt.position;
            }

            state.Transform.position = data.LookAt.position + data.StartOffset.Value;

            return true;
        }
    }
}
