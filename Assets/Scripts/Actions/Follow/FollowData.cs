﻿using UnityEngine;

namespace Assets.Scripts.Actions.Follow
{
    public class FollowData : MonoBehaviour
    {
        public string Target = "Player";

        [HideInInspector]
        public Transform LookAt = null;
        
        [HideInInspector]
        public Vector3? StartOffset = null;
    }
}
