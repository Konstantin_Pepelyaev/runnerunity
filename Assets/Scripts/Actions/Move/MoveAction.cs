﻿using Assets.Scripts.Actions.AI;
using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.Move
{
    [CreateAssetMenu(menuName = "Runner/Actions/Move")]
    public class MoveAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var c = GetComponent<CharacterController>(state);
            if(!c.isGrounded)
                return true;

            var loco = GetComponent<MoveData>(state);
            var movement = new Vector3(0f, -0.1f, 0f);

            var input = GetComponent<InputData>(state);
            movement.x = input.DodgeAmount * loco.MoveSpeed;
            movement.z = loco.MoveSpeed;

            c.Move(movement * blackboard.DeltaTime);
            
            return true;
        }
    }
}