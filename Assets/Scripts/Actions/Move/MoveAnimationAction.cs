﻿using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.Move
{
    [CreateAssetMenu(menuName = "Runner/Actions/MoveAnimation")]
    public class MoveAnimationAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var loco = GetComponent<MoveData>(state);

            var animator = GetComponent<Animator>(state);
            var c = GetComponent<CharacterController>(state);

            animator.SetBool("Grounded", c.isGrounded);
            animator.SetFloat("MoveSpeed", loco.MoveSpeed);

            return true;
        }
    }
}
