﻿using Assets.Scripts.Actions.GenerateTile;
using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.WorldShift
{
    [CreateAssetMenu(menuName = "Runner/Actions/WorldShift")]
    public class WorldShiftAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var data = GetComponent<WorldShiftData>(state);
            var pos = blackboard.Player.transform.position;
            if (pos.z < data.ShiftThreshold) 
                return true;
            
            blackboard.Player.transform.position = new Vector3(pos.x, pos.y, pos.z - data.ShiftThreshold);

            var tileData = GetComponent<GenerateTileData>(state);
            foreach(var t in tileData.GeneratedTiles)
            {
                pos = t.transform.position;
                t.transform.position = new Vector3(pos.x, pos.y, pos.z - data.ShiftThreshold);
            }

            return true;
        }
    }
}
