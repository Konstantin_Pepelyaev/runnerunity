﻿using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Actions.Gravity
{
    [CreateAssetMenu(menuName = "Runner/Actions/Gravity")]
    public class GravityAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var c = GetComponent<CharacterController>(state);
            
            if(!c.isGrounded)
            {
                c.Move(Physics.gravity * blackboard.DeltaTime); // Oversimlified
            }
            
            return true;
        }
    }
}
