﻿using UnityEngine;

namespace Assets.Scripts.Actions.Gravity
{
    public class GravityData : MonoBehaviour
    {
        public Vector3 Gravity = Physics.gravity;

        [HideInInspector]
        public Vector3 GravityAmount = Vector3.zero;
    }
}
