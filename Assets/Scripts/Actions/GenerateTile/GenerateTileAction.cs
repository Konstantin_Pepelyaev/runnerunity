﻿using System.Linq;
using Assets.Scripts.StateSystem;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Actions.GenerateTile
{    
    [CreateAssetMenu(menuName = "Runner/Actions/GenerateTile")]
    public class GenerateTileAction : GameAction
    {
        public override bool Execute(StateComponent state, Blackboard blackboard)
        {
            var data = GetComponent<GenerateTileData>(state);
            if(CanRemoveTile(state, blackboard, data))
            {
                RemoveTile(state, data);
            }

            while(data.GeneratedTiles.Count < data.MaxTiles)
            {
                var go = AddTile(state, data);

                var tile = go.GetComponent<TileData>();
                GenerateObstacles(tile);
            }
            return true;
        }

        private bool CanRemoveTile(StateComponent state, Blackboard blackboard, GenerateTileData data)
        {
            if(data.GeneratedTiles.Count == 0)
                return false;

            var firstTileZ = data.GeneratedTiles[0].transform.position.z;
            var canRemoveZ = firstTileZ + data.TileSize + data.RemoveTileDelta;
            var playerZ = blackboard.Player.transform.position.z;
            return playerZ > canRemoveZ;
        }

        private GameObject AddTile(StateComponent state, GenerateTileData data)
        {
            var go = Instantiate(data.TilePrefab);
            go.transform.SetParent(state.Transform);

            var position = new Vector3(0, 0, data.TileSize);
            var n = data.GeneratedTiles.Count;
            if(n > 0)
            {
                var lastTileTransform = data.GeneratedTiles[n - 1].transform;
                position += lastTileTransform.position;
            }

            go.transform.position = position;
            
            data.GeneratedTiles.Add(go);
            return go;
        }

        private void GenerateObstacles(TileData tileData)
        {
           foreach(var a in tileData.Anchors)
           {
               var n = tileData.GeneratedObstacles.Count;
               if(n >= tileData.MaxObstacles)
                   continue;
               
               var nextObstaclePrefabs = tileData.ObstaclePrefabs;
               var lastObstacle = tileData.GeneratedObstacles.LastOrDefault();
               if(lastObstacle)
               {
                   var lastObstacleData = lastObstacle.GetComponent<ObstacleData>();
                   nextObstaclePrefabs = tileData.ObstaclePrefabs.FindAll(
                       o => o.GetComponent<ObstacleData>().Placement != lastObstacleData.Placement
                            );
               }
               
               var rnd = Random.Range(0.0f, 1.0f);
               if(rnd <= tileData.ChanceNoObstacle)
                   continue;

               var id = Random.Range(0, nextObstaclePrefabs.Count);
               var nextPrefab = nextObstaclePrefabs[id];
               var go = CreateObstacle(a.transform, nextPrefab);
               tileData.GeneratedObstacles.Add(go);
           }
        }

        private GameObject CreateObstacle(Transform anchor, GameObject prefab)
        {
            var go = Instantiate(prefab);
            go.transform.SetParent(anchor);
            go.transform.localPosition = Vector3.zero;
            return go;
        }

        private void RemoveTile(StateComponent state, GenerateTileData data)
        {
            var tile = data.GeneratedTiles[0];
            data.GeneratedTiles.Remove(tile);
            Destroy(tile);
        }
    }
}
