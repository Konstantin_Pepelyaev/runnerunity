﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Actions.GenerateTile
{
    public class GenerateTileData : MonoBehaviour
    {
        public GameObject TilePrefab;

        public int TileSize = 10;
        public int MaxTiles = 7;

        public int RemoveTileDelta = 5;
        public List<GameObject> GeneratedTiles = new List<GameObject>();
    }
}
