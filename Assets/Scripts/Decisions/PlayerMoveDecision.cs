﻿using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Decisions
{
    [CreateAssetMenu(menuName = "Runner/Decisions/PlayerMove")]
    public class PlayerMoveDecision : Decision
    {
        public override bool Decide(StateComponent state, Blackboard blackboard)
        {
            var c = GetComponent<CharacterController>(state);
            return c.isGrounded;
        }
    }
}
