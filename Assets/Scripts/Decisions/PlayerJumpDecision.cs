﻿using Assets.Scripts.Actions.AI;
using Assets.Scripts.StateSystem;
using UnityEngine;

namespace Assets.Scripts.Decisions
{
    [CreateAssetMenu(menuName = "Runner/Decisions/PlayerJump")]
    public class PlayerJumpDecision : Decision
    {
        public override bool Decide(StateComponent state, Blackboard blackboard)
        {
            var input = GetComponent<InputData>(state);
            var c = GetComponent<CharacterController>(state);
            return c.isGrounded && input && input.Jump;
        }
    }
}
