﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Util
{
    public class TileData : MonoBehaviour
    {
        public GameObject[] Anchors;
        
        public List<GameObject> ObstaclePrefabs = new List<GameObject>();
        
        public List<GameObject> GeneratedObstacles = new List<GameObject>();
        
        public int MaxObstacles = 2;
        public float ChanceNoObstacle = 0.5f;
    }
}
