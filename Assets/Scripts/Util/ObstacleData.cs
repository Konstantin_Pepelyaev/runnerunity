﻿using UnityEngine;

namespace Assets.Scripts.Util
{
    public enum Placement
    {
        Left,
        Right,
        Middle
    }

    public class ObstacleData : MonoBehaviour
    {
        public Placement Placement;
    }
}
