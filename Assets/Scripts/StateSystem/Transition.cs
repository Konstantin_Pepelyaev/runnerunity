﻿namespace Assets.Scripts.StateSystem
{
    [System.Serializable]
    public class Transition
    {
        public Decision Decision;
        public State TrueState;
    }
}
