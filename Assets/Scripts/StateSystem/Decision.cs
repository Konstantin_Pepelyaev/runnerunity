﻿using UnityEngine;

namespace Assets.Scripts.StateSystem
{
    public abstract class Decision : ScriptableObject {

	    public abstract bool Decide(StateComponent state, Blackboard blackboard);
        
        protected T GetComponent<T>(StateComponent state)
        {
            return state.StateData.gameObject.GetComponent<T>();
        }
    }
}
