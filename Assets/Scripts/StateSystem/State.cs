﻿using UnityEngine;

namespace Assets.Scripts.StateSystem
{
    [CreateAssetMenu(menuName = "Runner/State")]
    public class State : ScriptableObject
    {   
        public GameAction[] Actions;
        public Transition[] Transitions;
    }
}
