﻿using Unity.Entities;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

namespace Assets.Scripts.StateSystem
{
    public struct StateComponent
    {
        public StateData StateData;
        public Transform Transform;
    }

    // TODO: use different update cycle stages for different actions
    [UpdateAfter(typeof(FixedUpdate))]
    public class StateSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var blackboard = new Blackboard
            {
                Player = GameObject.FindGameObjectWithTag("Player"),
                DeltaTime = Time.deltaTime
            };

            var entities = GetEntities<StateComponent>();
            foreach (var e in entities)
            {
                foreach (var action in e.StateData.State.Actions)
                {
                    action.Execute(e, blackboard);
                }

                foreach(var transition in e.StateData.State.Transitions)
                {
                    if(!transition.Decision)
                        continue;

                    var success = transition.Decision.Decide(e, blackboard);
                    if (!success) 
                        continue;

                    e.StateData.State = transition.TrueState;
                    break;
                }
            }
        }
    }
}
