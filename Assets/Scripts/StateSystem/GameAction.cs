﻿using System;
using UnityEngine;

namespace Assets.Scripts.StateSystem
{
    public abstract class GameAction : ScriptableObject
    {
        public abstract bool Execute(StateComponent state, Blackboard blackboard);

        protected T GetComponent<T>(StateComponent state)
        {
            return state.StateData.gameObject.GetComponent<T>();
        }
    }
}
