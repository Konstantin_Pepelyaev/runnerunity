﻿using UnityEngine;

namespace Assets.Scripts.StateSystem
{
    public class Blackboard
    {
        public GameObject Player; 
        public float DeltaTime;
    }
}
